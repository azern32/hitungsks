class Makul{
  constructor(kode,nama,sks,semester,parent,deskripsi){
    this._kode = String(kode);
    this._nama = String(nama);
    this._sks  = Number(sks);
    this._semester = Number(semester);
    if (parent) {
      this._parent = parent;
    };
    if (deskripsi) {
      this._deskripsi = deskripsi;
    }
  };

  get kode(){
    return this._kode;
  };

  get name(){
    return this._nama;
  };

  get sks(){
    return this._sks;
  };

  get semester(){
    return this._semester;
  };

  get parent(){
    return this._parent;
  };

  get deskripsi(){
    return this._deskripsi;
  };

  set sks(x){
    this._sks = x;
  };

  set semester(x){
    this._semester = x;
  };

  set parent(kode_makul){
    this._parent = [kode_makul];
  };

  set deskripsi(x){
    this._deskripsi = x;
  };

  set addParent(kode_makul){
    this._parent.push(kode_makul)
  };
};

class Mahasiswa {
  constructor(nim,nama,angkatan) {
    this._nim = nim;
    this._nama = String(nama);
    this._angkatan = angkatan;
  };
  get nim(){return this._nim};
  get nama(){return this._nam};
  get angkatan(){return this._angkatan};
};

class Staff {
  constructor(nip,nama,pos) {
    this._nip = nip;
    this._nama = String(nama);
    this._pos = String(pos);
  };
  get nip(){return this._nip};
  get nama(){return this._nama};
  get pos(){return this._pos};
};

function setMakultoFirebase(objectMakul) {
  firebase.database().ref('makul/' + objectMakul.kode).set(objectMakul);
};





var provider = new firebase.auth.GoogleAuthProvider();
var token;
var user;
function login() {
  firebase.auth().signInWithPopup(provider).then(function(result) {
    // This gives you a Google Access Token. You can use it to access the Google API.
    token = result.credential.accessToken;
    // The signed-in user info.
    user = result.user;
    // ...
  }).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;
    // The email of the user's account used.
    var email = error.email;
    // The firebase.auth.AuthCredential type that was used.
    var credential = error.credential;
    // ...
  });
}
