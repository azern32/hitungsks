var home="index.html",
	profil1="kondisidesa.html",
	profil2="saranaprasarana.html",
	about="about.html";

var navbar = `<div class="container">`
+      `<!-- Brand -->`
+      `<a class="navbar-brand" href="#">`
+       `<strong>ABBANUANG</strong>`
+      `</a>`
+      `<!-- Collapse -->`
+      `<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">`
+        `<span class="navbar-toggler-icon"></span>`
+      `</button>`
+      `<!-- Links -->`
+      `<div class="collapse navbar-collapse" id="navbarSupportedContent">`
+        `<!-- Left -->`
+        `<ul class="navbar-nav mr-auto">`
+          `<li class="nav-item">`
+            `<a class="nav-link" href="${home}">Halaman Utama<span class="sr-only">(current)</span>`
+            `</a>`
+          `</li>`
+          `<li class="nav-item">`
+            `<a class="nav-link" href="${profil1}">Kondisi Desa</a>`
+          `</li>`
+		  `<li class="nav-item">`
+            `<a class="nav-link" href="${profil2}">Sarana & Prasarana</a>`
+          `</li>`
+        `</ul>`
+        `<!-- Right -->`
+        `<ul class="navbar-nav nav-flex-icons">`
+          `<li class="nav-item">`
+            `<a href="${about}" class="nav-link border border-light rounded" target="_blank">`
+              `Tentang Kami`
+            `</a>`
+          `</li>`
+        `</ul>`
+      `</div>`
+   `</div>`
;

var credits = `<div class="container">`
+		`<a href="${about}" alt="Tentang Kami">KKN Unhas Gelombang 102</a>`
+		`<div class="credits">Desa Abbanuang, Kecamatan Awangpone, Kabupaten Bone</div>`
+    `</div>`
;

$('[name=navbar]').html(navbar);
$('[name=credits]').html(credits);





//////////////////////////////////////////////////////////
//// Daftar file-file gambar yang mau dipake ////////////
////////////////////////////////////////////////////////

// Halaman utama ///////////////////////////////////////
var main_slide1="1.jpg",
	main_slide2="2.jpg",
	main_slide3="4.jpg";

$('[name=main_slide1]').css('background-image',`url(assets/img/perekonomian/${main_slide1})`);
$('[name=main_slide2]').css('background-image',`url(assets/img/perekonomian/${main_slide2})`);
$('[name=main_slide3]').css('background-image',`url(assets/img/perekonomian/${main_slide3})`);

// Halaman Kondisi Desa ////////////////////////////////
var kondisi_slide1="",
	kondisi_slide2="",
	kondisi_slide3="";



// Halaman Sarana Prasarana ////////////////////////////
var sarana_slide1="",
	sarana_slide2="",
	sarana_slide3="";



// Halaman About ///////////////////////////////////////
var about_landing="",
	about_everyone="",
	about_member_1="gary.png",
	about_member_2="risda.png",
	about_member_3="pipin.png",
	about_member_4="icha.png",
	about_member_5="romla.png",
	about_member_6="rinda.png",
	about_member_7="fekis.png",
	about_member_8="ical.png",
	about_member_9="awanama.png";

$(`[name=member1_foto]`).attr('src',`assets/img/foto/${about_member_1}`);
$(`[name=member2_foto]`).attr('src',`assets/img/foto/${about_member_2}`);
$(`[name=member3_foto]`).attr('src',`assets/img/foto/${about_member_3}`);
$(`[name=member4_foto]`).attr('src',`assets/img/foto/${about_member_4}`);
$(`[name=member5_foto]`).attr('src',`assets/img/foto/${about_member_5}`);
$(`[name=member6_foto]`).attr('src',`assets/img/foto/${about_member_6}`);
$(`[name=member7_foto]`).attr('src',`assets/img/foto/${about_member_7}`);
$(`[name=member8_foto]`).attr('src',`assets/img/foto/${about_member_8}`);
$(`[name=member9_foto]`).attr('src',`assets/img/foto/${about_member_9}`);

var gary	=['Gary','Teknik Sipil','Koordinator'],
	risda	=['Risda','Peternakan','Sekretaris'],
	pipin	=['Pipin','Ilmu Ekonomi','Bendahara'],
	icha	=['Icha','Pemanfaatan Sumber Daya Perikanan','Anggota'],
	romla	=['Romla','Sastra Perancis','Anggota'],
	rinda	=['Rinda','Hukum Administrasi Negara','Anggota'],
	fekis	=['Fekis','Teknik Sipil','Anggota'],
	ical	=['Ical','Antropologi Sosial','Anggota'],
	awanama	=['Alamsyah','Psikologi','Anggota'];


for (i=1;i<10;i++){
	let arr = [gary,risda,pipin,icha,romla,rinda,fekis,ical,awanama];
	$(`[name=member${i}_name]`).text(arr[i-1][0]);
	$(`[name=member${i}_stats]`).text(arr[i-1][1]);
	$(`[name=member${i}_jab]`).text(arr[i-1][2]);
}

var aboutus_paragraf_1 = "",
	aboutus_paragraf_2 = "",
	aboutus_paragraf_3 = "";

var aboutus_title_1 ="",
	aboutus_title_2 ="",
	aboutus_title_3 ="";


	$(`[name=aboutus_title_1]`).text(aboutus_title_1);
	$(`[name=aboutus_title_2]`).text(aboutus_title_2);
	$(`[name=aboutus_title_3]`).text(aboutus_title_3);

	$(`[name=aboutus_paragraf_1]`).text(aboutus_paragraf_1);
	$(`[name=aboutus_paragraf_2]`).text(aboutus_paragraf_2);
	$(`[name=aboutus_paragraf_3]`).text(aboutus_paragraf_3);






//////////////////////////////////////////////////////////
//// Preview Gambar /////////////////////////////////////
////////////////////////////////////////////////////////
